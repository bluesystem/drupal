# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

-   Basse drupal repository with drupal4docker
-   1.0
-   [Learn Drupal4docker](https://wodby.com/docs/stacks/drupal/local)

### How do I get set up?

-   git clone https://bluesys@bitbucket.org/bluesystem/drupal.git
-   Set up .env file (Project name and url)
-   composer install
-   (sudo) make up
-   mkdir -m 777 web/sites/default/files
-   mkdir -m 777 web/sites/default/files/translations
-   cp web/sites/default/default.settings.php web/sites/default/settings.php
-   chmod 777 web/sites/default/settings.php
-   mkdir -m 777 config/sync
-   navigate <yoursite>:8000
-   chmod 444 web/sites/default/settings.php
-   sudo rm -R .git
-   git init

### ... BONNE CHANCE
